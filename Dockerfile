FROM alpine

ENV DATOMIC_INSTALL_DIR="/var/lib/datomic"
ENV DATOMIC_VERSION="0.9.5703.21"
ENV STORAGE_ACCESS="remote"

RUN apk update && apk add bash openjdk8 pwgen

COPY ./entrypoint.sh /tmp/entrypoint.sh

RUN addgroup -S datomic && \
    adduser -S -D -H -G datomic -s /sbin/nologin datomic && \
    mkdir /var/lib/datomic && \
    chown -R datomic:datomic /var/lib/datomic && \
    chmod +x /tmp/entrypoint.sh

EXPOSE 4334 4335

# VOLUME ["/var/lib/datomic"]

ENTRYPOINT ["/tmp/entrypoint.sh"]

# CMD ["/tmp/entrypoint.sh"]