#!/bin/bash

set -e

_term() {
    echo "Caught SIGTERM signal!"
    childs=$(pstree -p $child | grep -o [0-9]* | sort -r -g)
    for pid in $childs 
    do
	# wait $pid
	kill -- $pid 2>/dev/null
	echo "kill pid $pid"

    done
    exit 0;
}

trap _term SIGTERM

TMP_TRANSACTOR_PROPERTIES="/tmp/transactor.properties"
TRANSACTOR_PROPERTIES="${DATOMIC_INSTALL_DIR}/transactor.properties"

init-file-transactor-properties () {
    cat <<EOM > ${TRANSACTOR_PROPERTIES}
    protocol=free
    host=0.0.0.0
    alt-host=127.0.0.1
    port=4334
    h2-port=4335

    memory-index-threshold=32m
    memory-index-max=128m
    object-cache-max=128m

    data-dir=data
    log-dir=log
    pid-file=log/transactor.pid

    # write-concurrency=4
    # read-concurrency=8

EOM

    if [ "${STORAGE_ADMIN_PASSWORD}" ]; then
	echo "storage-admin-password =${STORAGE_ADMIN_PASSWORD}" \
	     >> ${TRANSACTOR_PROPERTIES}

    else
	GEN_STORAGE_ADMIN_PASS=$(pwgen -s 16 1)
	echo "storage-admin-password=${GEN_STORAGE_ADMIN_PASS}" \
	     >> ${TRANSACTOR_PROPERTIES}
	
	echo "storage-admin-password = ${GEN_STORAGE_ADMIN_PASS}"
    fi

    if [ "${STORAGE_DATOMIC_PASSWORD}" ]; then
	echo "storage-datomic-password=${STORAGE_DATOMIC_PASSWORD}" \
	     >> ${TRANSACTOR_PROPERTIES}
    else
	GEN_STORAGE_PASS=$(pwgen -s 16 1)
	echo "storage-datomic-password=${GEN_STORAGE_PASS}" \
	     >> ${TRANSACTOR_PROPERTIES}

	echo "storage-datomic-password = ${GEN_STORAGE_PASS}"
    fi

    echo "storage-access=${STORAGE_ACCESS}" >> ${TRANSACTOR_PROPERTIES}

}

init-new-datomic-free () {
    echo "downloading datomic free ${DATOMIC_VERSION}"
    wget https://my.datomic.com/downloads/free/${DATOMIC_VERSION} -O /tmp/datomic.zip

    echo "unzipping datomic free ${DATOMIC_VERSION}"
    unzip -d /tmp /tmp/datomic.zip

    echo "moving extracted datomic to directory ${DATOMIC_INSTALL_DIR}"
    mv /tmp/datomic-free-${DATOMIC_VERSION}/* ${DATOMIC_INSTALL_DIR}/
    chown -R datomic:datomic ${DATOMIC_INSTALL_DIR}
}

if [ ! "$(ls -A ${DATOMIC_INSTALL_DIR})" ]; then
    echo "datomic directory no exists. Init new datomic free"
    init-new-datomic-free
fi

if [ -e "${TMP_TRANSACTOR_PROPERTIES}" ]; then

    echo "copying transactor.properties to ${TRANSACTOR_PROPERTIES}"
    cp ${TMP_TRANSACTOR_PROPERTIES} ${TRANSACTOR_PROPERTIES}
    chown datomic:datomic ${TRANSACTOR_PROPERTIES}

else

    [ ! -e "${TRANSACTOR_PROPERTIES}" ] && \
	echo "create a new ${TRANSACTOR_PROPERTIES}"
    init-file-transactor-properties
    chown datomic:datomic ${TRANSACTOR_PROPERTIES}

fi

${DATOMIC_INSTALL_DIR}/bin/transactor ${DATOMIC_INSTALL_DIR}/transactor.properties &

child=${!}
wait $child

exit $?
